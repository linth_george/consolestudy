import asyncio
import functools
from asyncio import Future

# # Future Callbacks

# In addition to working like a coroutine,
# a ``Future`` can invoke callbacks when it is completed.
# Callbacks are invoked in the order they are registered.

# Set the callback of ``Future`` by involving ``add_done_callback()``
# and passing a callback function (a Callable)

# The callback should expect one argument, the ``Future`` instance.
# To pass additional arguments to the callbacks, use ``functools.partial()`` to create a wrapper.


def callback(future: Future, n):
    print('{}: future done: {}'.format(n, future.result()))


async def register_callbacks(all_done2: Future):
    """
    a Coroutine used to register a callback to the given Future object
    :param all_done2: the Future object
    :return:
    """
    print('registering callbacks on future')
    all_done2.add_done_callback(functools.partial(callback, n=1))
    all_done2.add_done_callback(functools.partial(callback, n=2))


async def main(all_done1: Future):
    await register_callbacks(all_done1)
    print('setting result of future')
    all_done1.set_result('the result')


event_loop = asyncio.get_event_loop()
try:
    all_done = asyncio.Future()
    print('entering event loop')
    event_loop.run_until_complete(main(all_done))
finally:
    event_loop.close()
    print('closing event loop')
