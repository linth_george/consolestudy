
# Iterating over the network yields addresses,
# but not all of them are valid for hosts.
# For example, the base address of the network and
# the broadcast address are both included.
# To find the addresses that can be used by regular hosts on the network,
# use the ``hosts()`` method, which produces a generator.

# Comparing the output of this example with the previous example shows that
# the host addresses do not include the first values produced
# when iterating over the entire network.


import ipaddress
from collections import deque

try:
    net = ipaddress.ip_network('192.168.4.1/23') # will yield error
except as e:

net = ipaddress.ip_network('192.168.2.0/23')
print('{!r}'.format(net))
for ip in net.hosts():
    print(ip)
print()

# this only works out for the IPv4Network
dd = deque(net.hosts(), maxlen=1)
last_element = dd.pop()
print('last item: {}'.format(last_element))
