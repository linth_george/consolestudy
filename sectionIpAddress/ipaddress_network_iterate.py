import ipaddress


NETWORKS = [
    '10.9.0.0/24',
    'fdfd:87b5:b475:5e3e::/64',
]

for n in NETWORKS:
    # parsing and output either ``IPv4Network`` or ``IPv6Network``
    net = ipaddress.ip_network(n)
    print('{!r}'.format(net))
    # A network instance is iterable, and yields the addresses on the network!!
    # This example only prints a few of the addresses,
    # because an IPv6 network can contain far more addresses than fit in the output.
    for i, ip in zip(range(3), net):
        print(ip)
    print()
