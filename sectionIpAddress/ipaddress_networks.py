
# # Networks
# A network is defined by a range of addresses.
# It is usually expressed with a __base address__ and
# a __mask__ indicating which portions of the address represent the network,
# and which portions are remaining to represent addresses on that network.
# The mask can be expressed explicitly,
# or using a prefix length value as in the example below.

# As with addresses, there are two network classes for IPv4 and IPv6 networks.
# Each class provides properties or methods for
# accessing values associated with the network
# such as the broadcast address and the addresses on the network available for hosts to use.

import ipaddress

NETWORKS = [
    '10.9.0.0/24',
    'fdfd:87b5:b475:5e3e::/64',
]

for n in NETWORKS:
    # parsing and output either ``IPv4Network`` or ``IPv6Network``
    net = ipaddress.ip_network(n)
    print('{!r}'.format(net))
    print('     is private:', net.is_private)
    print('      broadcast:', net.broadcast_address)
    print('     compressed:', net.compressed)
    print('   with netmask:', net.with_netmask)
    print('  with hostmask:', net.with_hostmask)
    print('  num addresses:', net.num_addresses)
    print()
