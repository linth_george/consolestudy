

# # Addresses
# The most basic object represents the network address itself.
# Pass a string, integer, or byte sequence to ip_address()
# to construct an address. The return value will be
# an IPv4Address or IPv6Address instance,
# depending on the type of address being used.

# Both classes can provide various representations of the address
# for different purposes, as well as answer basic assertions
# such as whether the address is reserved for multicast communication
# or if it is on a private network.


import binascii
import ipaddress


ADDRESSES = [
    '10.9.0.6',
    'fdfd:87b5:b475:5e3e:b1bc:e121:a8eb:14aa',
]

for ip in ADDRESSES:
    # parsing and output either ``IPv4Address`` or ``IPv6Address``
    addr = ipaddress.ip_address(ip)
    print('{!r}'.format(addr))
    print('   IP version:', addr.version)
    print('   is private:', addr.is_private)
    print('  packed form:', binascii.hexlify(addr.packed))
    print('      integer:', int(addr))
    print()
