import asyncio
from asyncio import Task

# Catching the exception provides an opportunity to clean up work already done, if necessary.


async def task_func():
    """
    a Coroutine wrapped in a task
    """
    print('in task_func, sleeping 10 seconds')
    try:
        await asyncio.sleep(10)
    except asyncio.CancelledError:
        print('task_func was canceled')
        raise
    return 'the result'


def task_canceller(task: Task):
    """
    a Callable used to cancel the given Task object
    :param task: the Task object
    :return:
    """
    print('in task_canceller')
    task.cancel()
    print('canceled the task')


async def main(loop):
    print('creating task')
    task = loop.create_task(task_func())
    # loop.call_soon(task_canceller, task)
    loop.call_later(3, task_canceller, task)
    try:
        await task
    except asyncio.CancelledError:
        print('main() also sees task as canceled')


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(event_loop))
finally:
    event_loop.close()
    print('closing event loop')
