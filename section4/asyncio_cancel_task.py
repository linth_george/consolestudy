import asyncio

# # Canceling a Task

# By retaining the ``Task`` object returned from ``create_task()``,
# it is possible to cancel the operation of the task before it completes.

# This example creates and then cancels a task before starting the event loop.
# The result is a ``CancelledError`` exception from ``run_until_complete()``.


async def task_func():
    """
    a Coroutine wrapped in a task
    """
    print('in task_func#1')
    await asyncio.sleep(10)
    print('in task_func#2')
    return 'the result'


async def main(loop):
    print('creating task')
    task = loop.create_task(task_func())

    print('canceling task')
    task.cancel()

    print('canceled task {!r}'.format(task))
    try:
        await task
    except asyncio.CancelledError:
        print('caught error from canceled task')
    else:
        print('task result: {!r}'.format(task.result()))


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(event_loop))
finally:
    event_loop.close()
    print('closing event loop')
