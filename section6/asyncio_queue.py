import asyncio
from asyncio import Queue
# # Queues

# An ``asyncio.Queue`` provides a first-in, first-out data structure
# for coroutines like a queue. ``Queue`` does for threads or a multiprocessing.
# ``Queue`` does for processes.

# Adding items with ``put()`` or removing items with ``get()``
# are both asynchronous operations, since the queue size might be fixed
# (blocking an addition) or the queue might be empty
# (blocking a call to fetch an item).


async def consumer(queue: Queue, n: int):
    """
    a Coroutine stands for consumer
    """
    print('consumer {}: starting'.format(n))
    while True:
        print('consumer {}: waiting for item'.format(n))
        item = await queue.get()
        print('consumer {}: has item {}'.format(n, item))
        if item is None:
            # None is the signal to stop.
            queue.task_done()
            break
        else:
            await asyncio.sleep(0.01 * item)
            queue.task_done()
    print('consumer {}: ending'.format(n))


async def producer(queue: Queue, num_workers: int):
    """
    a Coroutine stands for producer
    """
    print('producer: starting')
    # Add some numbers to the queue to simulate jobs
    for i in range(num_workers * 3):
        await queue.put(i)
        print('producer: added task {} to the queue'.format(i))
    # Add None entries in the queue
    # to signal the consumers to exit
    print('producer: adding stop signals to the queue')
    for i in range(num_workers):
        await queue.put(None)
    print('producer: waiting for queue to empty')
    await queue.join()
    print('producer: ending')


async def main(loop, num_consumers: int):
    # Create the queue with a fixed size so the producer
    # will block until the consumers pull some items out.
    queue = asyncio.Queue(maxsize=num_consumers)

    # Scheduled the consumer tasks.
    consumers = [
        loop.create_task(consumer(queue, i))
        for i in range(num_consumers)
    ]

    # Schedule the producer task.
    prod = loop.create_task(producer(queue, num_consumers))

    # Wait for all of the coroutines to finish.
    await asyncio.wait(consumers + [prod])


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(event_loop, 2))
finally:
    event_loop.close()
    print('closing event loop')
