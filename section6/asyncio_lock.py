import asyncio
from asyncio import Lock
import functools

# Synchronization Primitives

# Although asyncio applications usually run as a single-threaded process,
# they are still built as concurrent applications.
# Each coroutine or task may execute in an unpredictable order,
# based on delays and interrupts from I/O and other external events.
# To support safe concurrency, ``asyncio`` includes
# implementations of some of the same low-level primitives
# found in the __threading__ and __multiprocessing__ modules.

# # Locks

# A Lock can be used to guard access to a shared resource.
# Only the holder of the lock can use the resource.
# Multiple attempts to acquire the lock will block
# so that there is only one holder at a time.

# A lock's ``acquire()`` method can be invoked directly, using ``await``,
# and calling the ``release()`` method when done as in ``coro2()`` in this example.
# They also can be used as asynchronous context managers
# with the with ``await`` keywords, as in ``coro1()``.


def unlock(lock: Lock):
    print('callback releasing lock')
    lock.release()


async def coro1(lock: Lock):
    print('coroutine1 waiting for the lock')
    async with lock:
        print('coroutine1 acquired lock')
    print('coroutine1 released lock')


async def coro2(lock: Lock):
    print('coroutine2 waiting for the lock')
    await lock.acquire()
    try:
        print('coroutine2 acquired lock')
    finally:
        print('coroutine2 released lock')
        lock.release()


async def main(loop):
    # Create and acquire a shared lock.
    lock = asyncio.Lock()
    print('acquiring the lock before starting coroutines')
    await lock.acquire()
    print('lock acquired: {}'.format(lock.locked()))

    # Schedule a callback to unlock the lock.
    loop.call_later(1, functools.partial(unlock, lock))

    # Run the coroutines that want to use the lock.
    print('waiting for coroutines')
    await asyncio.wait([coro1(lock), coro2(lock)]),


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(event_loop))
finally:
    event_loop.close()
    print('closing event loop')

