# [Class Attribute vs. Instance Attribute In Python: What You Might Have Missed]
# (https://dzone.com/articles/python-class-attributes-vs-instance-attributes)


class ExampleClass(object):
    # the class attribute
    class_attr = 0

    def __init__(self, instance_attr):
        # the instance attribute
        self.instance_attr = instance_attr


if __name__ == '__main__':
    foo = ExampleClass(1)
    bar = ExampleClass(2)
    # print the instance attribute of the object foo
    print(foo.instance_attr)
    #1
    #print the instance attribute of the object var
    print(bar.instance_attr)
    #2
    #print the class attribute of the class ExampleClass as a property of the class itself
    print(ExampleClass.class_attr)
    #0
    #print the classattribute  of the class as a proporty of the objects foo,bar
    print(bar.class_attr)
    #0
    print(foo.class_attr)
    #0
    # try to print instance attribute as a class property
    try:
        print(ExampleClass.instance_attr)
    except AttributeError as e:
        # AttributeError: type object 'ExampleClass' has no attribute 'instance_attr'
        print('AttributeError for authenticate:', e)

    # modify the class attribute as a foo property
    foo.class_attr = 5
    print(foo.class_attr)
    # 5
    # print the class attribute as a property of a bar
    print(bar.class_attr)
    # 0
