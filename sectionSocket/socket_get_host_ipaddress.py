
# Python Program to Get IP Address
# https://www.geeksforgeeks.org/python-program-find-ip-address/

import ipaddress
import socket
from ipaddress import IPv4Address, IPv4Interface, IPv4Network

hostname = socket.gethostname()
host_ip = socket.gethostbyname(hostname)
print("Your Computer Name is: " + hostname)
print("Your Computer IP Address is: {}, type: {}".format(host_ip, type(host_ip)))
ip_address = ipaddress.ip_address(host_ip)
print('{!r}'.format(ip_address))
print('   IP version:', ip_address.version)
print('   is private:', ip_address.is_private)
print('      integer:', int(ip_address))
print()
#subnet = ipaddress.ip_network(host_ip)
interface = ipaddress.ip_interface(host_ip)
print('{!r}'.format(interface))
print('network:\n  ', interface.network)
print('ip:\n  ', interface.ip)
print('IP with prefixlen:\n  ', interface.with_prefixlen)
print('netmask:\n  ', interface.with_netmask)
print('hostmask:\n  ', interface.with_hostmask)
print()
