import asyncio

# Composing Coroutines with Control Structures

# Linear control flow between a series of coroutines is easy to manage
# with the built-in language keyword ``await``.
# More complicated structures allowing one coroutine to wait for several others
# to complete in parallel are also possible using tools in asyncio.

# # Waiting for Multiple Coroutines
# It is often useful to divide one operation into many parts and execute them separately.
# For example, downloading several remote resources or querying remote APIs.
# In situations where the order of execution doesn't matter,
# and where there may be an arbitrary number of operations,
# ``wait()`` can be used to pause one coroutine until the other background operations complete.

# Internally, ``wait()`` uses a ``set`` to hold the ``Task`` instances it creates.
# This results in them starting, and finishing, in an unpredictable order.
# The return value from ``wait()`` is a ``tuple``
# containing two ``sets`` holding the __finished__ and __pending__ tasks.


async def phase(i: int):
    """
    the sub-Coroutine that runs in parallel way with other sub-Coroutines
    """
    print('in phase {}'.format(i))
    await asyncio.sleep(1 * i)
    print('done with phase {}'.format(i))
    return 'phase {} result'.format(i)


async def main(num_phases: int):
    """
    the main Coroutine that waits the result of task to be produced
    """
    print('starting main')
    # phases = [phase(i) for i in range(num_phases)]
    phases = [
        phase(i)
        for i in range(num_phases)
    ]
    print('waiting for phases to complete')
    completed, pending = await asyncio.wait(phases)
    # results = [t.result() for t in completed]
    results = [
        t.result()
        for t in completed
    ]
    print('results: {!r}'.format(results))


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(3))
finally:
    event_loop.close()
    print('closing event loop')
