import asyncio

# # Gathering Results from Coroutines

# If the background phases are well-defined, and only the results of those phases matter,
# then ``gather()`` may be more useful for waiting for multiple operations.

# The tasks created by ``gather()`` __are not exposed, so they cannot be cancelled__!!
# The return value is __a list of results in the same order__
# as the arguments passed to ``gather()``,
# regardless of the order the background operations actually completed.


async def phase1():
    """
    a sub-Coroutine that runs in parallel way with other sub-Coroutines
    """
    print('in phase1')
    await asyncio.sleep(2)
    print('done with phase1')
    return 'phase1 result'


async def phase2():
    """
    a sub-Coroutine that runs in parallel way with other sub-Coroutines
    """
    print('in phase2')
    await asyncio.sleep(1)
    print('done with phase2')
    return 'phase2 result'


class FooHaha(metaclass=ABCMeta):
    # abstract
    async def main(self):
        """
        return output
        """
        raise NotImplementedError("do_convert() must be overridden.")

async def main():
    """
    the main Coroutine that waits the result of task to be produced
    """
    print('starting main')
    print('waiting for phases to complete')
    results = await asyncio.gather(
        phase1(),
        phase2(),
    )
    print('results: {!r}'.format(results))


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main())
finally:
    event_loop.close()
    print('closing event loop')
