import asyncio

# There will only be pending operations left if ``wait()`` is used with a __timeout__ value.

# Those remaining background operations should either be cancelled or finished
# by waiting for them. Leaving them pending while the event loop continues
# will let them execute further, which may not be desirable
# if the overall operation is considered aborted.
# Leaving them pending at the end of the process will result in warnings being reported.


async def phase(i: int):
    """
    the sub-Coroutine that runs in parallel way with other sub-Coroutines
    """
    print('in phase {}'.format(i))
    try:
        await asyncio.sleep(1 * i)
    except asyncio.CancelledError:
        print('phase {} canceled'.format(i))
        raise
    else:
        print('done with phase {}'.format(i))
        return 'phase {} result'.format(i)


async def main(num_phases: int):
    """
    the main Coroutine that waits the result of task to be produced
    """
    print('starting main')
    phases = [
        phase(i)
        for i in range(num_phases)
    ]
    print('waiting 1 second for phases to complete')
    completed, pending = await asyncio.wait(phases, timeout=1)
    print('{} completed and {} pending'.format(
        len(completed), len(pending),
    ))
    # Cancel remaining tasks so they do not generate errors
    # as we exit without finishing them.
    if pending:
        print('canceling tasks')
        for t in pending:
            t.cancel()
    print('exiting main')


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(3))
finally:
    event_loop.close()
    print('closing event loop')
