from abc import ABCMeta
from typing import Generic, TypeVar


T = TypeVar("T")


class IeConversionDelegate(metaclass=ABCMeta):
    # abstract
    def do_convert(self, input_data: T) -> T:
        """
        return output
        """
        raise NotImplementedError("do_convert() must be overridden.")


class IeCallableDelegate(metaclass=ABCMeta):
    """
    added in 2020/08/05
    """
    # abstract
    def call(self) -> T:
        """
        return output
        """
        raise NotImplementedError("call() must be overridden.")


class IeFilterDelegate(metaclass=ABCMeta):
    """
    added in 2020/08/05
    """
    # abstract
    def predicate(self, item: T) -> bool:
        """
        return output
        """
        raise NotImplementedError("predicate() must be overridden.")


class IeDataHolder(Generic[T]):
    def __init__(self, kept_data: T):
        self.kept_data = kept_data


class IeChain(Generic[T]):
    def __init__(self, data: T):
        self.ie_data_holder = IeDataHolder(data)

    def map(self, conversion: IeConversionDelegate):
        """
        return IeChain
        """
        input_data = self.ie_data_holder.kept_data
        # print('type of input_data: {}'.format(type(input_data)))
        output_data = conversion.do_convert(input_data)
        # print('type of output_data: {}'.format(type(output_data)))
        return IeChain(output_data)

    def on_complete(self) -> T:
        return self.ie_data_holder.kept_data

