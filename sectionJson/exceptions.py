

# https://docs.python.org/3.3/tutorial/errors.html#tut-userexceptions
# https://www.programiz.com/python-programming/user-defined-exception
# https://www.codementor.io/sheena/how-to-write-python-custom-exceptions-du107ufv9
# https://franklingu.github.io/programming/2016/06/30/properly-reraise-exception-in-python/
class IeCustomException(Exception):
    def __init__(self, status_code: str, message: str, errors: Exception = None):
        self.status_code = status_code
        self.message = message
        self.errors = errors


def log_wrapper(*args, **kwargs):
    if len(kwargs) > 0:
        print(args, kwargs)
    else:
        print(args)


http_execute_request_failure = '99599'
# catch the {@link java.io.IOException} in Java
http_request_error = '99598'
http_response_error = '99597'
http_response_parsing_error = '99597'
http_illegal_argument_error = '99596'
http_wrong_status_code = '99595'

socket_error_common = '89999'
socket_error_write = '89998'
socket_error_read = '89997'
socket_error_disconnection = '89996'

internal_conversion_error = '99998'
internal_process_error = '99997'
