from .exceptions import IeCustomException, conversion_error
from .json_utils import object_to_json


class BaseDongleCommandData(object):
    def __init__(self):
        pass


class IeDongleCommand(object):
    def __init__(self, dtype: int, data: BaseDongleCommandData):
        self.dtype = dtype
        self.data = data

    def to_json(self):
        try:
            return object_to_json(self)
        except Exception as cause:
            raise IeCustomException(conversion_error, "Fail to convert json", cause)


class IeEmptyDongleCommandData(BaseDongleCommandData):
    def __init__(self):
        super().__init__()


class IeSiblingDongleCommandData(BaseDongleCommandData):
    def __init__(self, siblings: str):
        super().__init__()
        self.siblings = siblings
