from .dongle_commands import IeDongleCommand, IeEmptyDongleCommandData, IeSiblingDongleCommandData
from .json_utils import object_to_json


systemInfo = IeDongleCommand(114, IeEmptyDongleCommandData())
siblings = IeDongleCommand(116, IeSiblingDongleCommandData('DongleB999'))
print(object_to_json(systemInfo))
print(object_to_json(siblings))

# https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
# python3.8 -m sectionJson.object_to_json_test

