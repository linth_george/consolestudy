from datetime import timedelta, date, datetime


current_datetime: datetime = datetime.now()
ten_day_earlier: datetime = current_datetime + timedelta(days=-10)
ten_days_later: datetime = current_datetime + timedelta(days=10)
print('ten_day_earlier: {}, ten_days_later: {}'.format(ten_day_earlier, ten_days_later))
print('type of current_datetime: {}, current_datetime: {}'.format(type(current_datetime), current_datetime))

