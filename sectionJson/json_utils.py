import json


# [Convert Python Objects to JSON](http://tdongsi.github.io/blog/2016/05/21/convert-python-objects-to-json/)
def object_to_json(given: object) -> str:
    try:
        return json.dumps(given, default=vars, indent=4)
    except Exception as cause:
        print('error on object_to_json {}'.format(cause))
        return ''
