import asyncio

# # Returning Values from Coroutines

# In this case, run_until_complete() also returns the result of the coroutine it is waiting for.


async def coroutine() -> str:
    print('in coroutine')
    return 'You lose'


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    return_value = event_loop.run_until_complete(
        coroutine()
    )
    print('it returned: {!r}'.format(return_value))
finally:
    event_loop.close()
    print('closing event loop')
